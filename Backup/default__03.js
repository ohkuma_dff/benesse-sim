(function(){

	var ga = $(
			'<!-- Global site tag (gtag.js) - Google Analytics -->\n'+
			'<script async src="https://www.googletagmanager.com/gtag/js?id=UA-167603600-1"></script>\n'+
			'<script>\n'+
			'window.dataLayer = window.dataLayer || [];\n'+
			'function gtag(){dataLayer.push(arguments);}\n'+
			'gtag(\'js\', new Date());\n'+
			'\n'+
			'gtag(\'config\', \'UA-167603600-1\');\n'+
			'</script>'
		);
	$(ga).appendTo('head');

	//var currentScriptHostname = $('script[src*="default.js"]').attr('src').match(/(\/\/.*?\/)js\//)[1];
	var currentScriptHostname = '//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/';


	// URL PATH
	var pathname = location.pathname;

	// 日英判別
	var html_lang = $('html').attr('lang');
	var japaneseFlg = ( html_lang == 'ja') ? true : false;

	// HEAD META
	$('title').after('<meta name="msapplication-square70x70logo" content="https://www.benesse-hd.co.jp/small.jpg"><meta name="msapplication-square150x150logo" content="https://www.benesse-hd.co.jp/medium.jpg"><meta name="msapplication-square310x310logo" content="https://www.benesse-hd.co.jp/large.jpg"><meta name="msapplication-TileColor" content="#fff">')
	$('title').after('<link rel="icon" href="https://www.benesse-hd.co.jp/favicon.ico"><link rel="apple-touch-icon-precomposed" sizes="180x180" href="https://www.benesse-hd.co.jp/apple-touch-icon.png">')
	$('title').after('<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="format-detection" content="telephone=no">')

	// 元CSS-
	$('link[href^="/assets/applicatio"]').remove();//

	// メインCSS+
	$('head link:last').after('<link rel="stylesheet" href="' + currentScriptHostname + html_lang + '/hd-common/css/ss_suggest.css" media="all">');
	$('head link:last').after('<link rel="stylesheet" href="' + currentScriptHostname + html_lang + '/hd-common/css/common.css">');
	$('head link:last').after('<link rel="stylesheet" href="' + currentScriptHostname + html_lang + '/hd-common/css/style.css">');
	$('head link:last').after('<link rel="stylesheet" href="' + currentScriptHostname + html_lang + '/hd-common/css/sustainability.css">');
	$('head link:last').after('<link rel="stylesheet" href="' + currentScriptHostname + html_lang + '/hd-common/css/achieve.css">');

	var themesNum = pathname.match(/themes\/(\d{2,3})/);
	var sustainaTop = false;
	var sustainaData = false;
	var sustainaPreview = false;
	var sustainaAchievements = false;
	if( themesNum ){
		var themesId = themesNum[1];
		sustainaData = ( themesId>=119 && themesId<=124 ) ? true : false;// データ
		sustainaAchievements = ( themesId>=98 && themesId<=117 ) ? true : false;// 取り組み
	}else if(pathname.match(/responses\/\d/)){
		sustainaPreview = true;// プレビュー
	}else{
		sustainaTop =true;// サステナトップ
	}
	if(sustainaAchievements||sustainaPreview){
		$('head link:last').after('<link rel="stylesheet" href="' + currentScriptHostname + html_lang + '/hd-common/css/achieve.css?20200703">');
	}

	$(function(){

		$('body').addClass('bh_csr');

		$('.Wrapper').attr('id','wrapper').addClass(sustainaTop?'sustaina_top':'')
		// ヘッダー挿入
		.prepend((japaneseFlg ? headerInc_ja : headerInc_en));


		// 日英リンク
		/*
		if(pathname.indexOf('/en/themes/119')>=0){
			$('#lang, #lang2').attr('href', '/ja');

		}else if(pathname.indexOf('/ja/themes/127')>=0){
			$('#lang, #lang2').attr('href', '/en');

		}else{
			$('#lang, #lang2').attr('href', function(){ return (japaneseFlg ? location.href.replace('/ja','/en') : location.href.replace('/en','/ja'))});
		}
		*/
		$('#lang, #lang2').attr('href', function(){ return (japaneseFlg ? location.href.replace('/ja','/en') : location.href.replace('/en','/ja'))});


		fontSize();// フォントサイズ

		$('.Topics').attr('id','wrap')
		.addClass('id' + (sustainaTop?' sustaina_top':''))// id ??
		// フッター挿入
		.after((japaneseFlg ? footerInc_ja : footerInc_en));

		// 検索 suggest script
		var script = document.createElement('script');
		script.src = currentScriptHostname + html_lang + '/hd-common/js/ss_suggest_config.js';
		document.body.appendChild(script);


		// パンくず挿入
		var elm = "";
		var breadcrumb = "";
		if (japaneseFlg) {
			if (themesId == 141 || themesId == 96) {
				elm = '<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/management/index.html">サステナビリティマネジメント</a></li>'+
					  '<li id="active">ステークホルダーとのコミュニケーション</li>';
			} else if (themesId == 142) {
				elm = '<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/management/index.html">サステナビリティマネジメント</a></li>'+
					  '<li id="active">イニシアチブへの参加</li>';
			} else if (themesId == 145) {
				elm = '<li id="active">ベネッセグループのESG活動</li>';
			} else if (themesId == 146 || themesId == 111) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li id="active">環境</li>';
			} else if (themesId == 147 || themesId == 112) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/146">環境</a></li>'+
					  '<li id="active">環境マネジメント</li>';
			} else if (themesId == 148 || themesId == 113) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/146">環境</a></li>'+
					  '<li id="active">気候変動への対応</li>';
			} else if (themesId == 149 || themesId == 113) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/146">環境</a></li>'+
					  '<li id="active">資源の保全</li>';
			} else if (themesId == 150 || themesId == 114) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/146">環境</a></li>'+
					  '<li id="active">環境教育</li>';
			} else if (themesId == 151 || themesId == 107) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li id="active">社会</li>';
			} else if (themesId == 152 || themesId == 129) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/151">社会</a></li>'+
					  '<li id="active">人権の尊重</li>';
			} else if (themesId == 153 || themesId == 108) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/151">社会</a></li>'+
					  '<li id="active">ダイバーシティ&インクリュージョン</li>';
			} else if (themesId == 154 || themesId == 110) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/151">社会</a></li>'+
					  '<li id="active">働きやすく活気ある職場づくり</li>';
			} else if (themesId == 155 || themesId == 109) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/151">社会</a></li>'+
					  '<li id="active">人財の育成</li>';
			} else if (themesId == 156) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/151">社会</a></li>'+
					  '<li id="active">労働安全衛生</li>';
			} else if (themesId == 157 || themesId == 115) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/151">社会</a></li>'+
					  '<li id="active">地域社会</li>';
			} else if (themesId == 158) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/151">社会</a></li>'+
					  '<li id="active">品質・安全性の確保</li>';
			} else if (themesId == 159) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/151">社会</a></li>'+
					  '<li id="active">サプライチェーン・マネジメント</li>';
			} else if (themesId == 160) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/151">社会</a></li>'+
					  '<li id="active">情報セキュリティ</li>';
			} else if (themesId == 161) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li id="active">ガバナンス</li>';
			} else if (themesId == 162) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/161">ガバナンス</a></li>'+
					  '<li id="active">コーポレートガバナンス</li>';
			} else if (themesId == 163) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/161">ガバナンス</a></li>'+
					  '<li id="active">内部統制</li>';
			} else if (themesId == 164) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/161">ガバナンス</a></li>'+
					  '<li id="active">リスクマネジメント・コンプライアンス</li>';
			} else if (themesId == 165) {
				elm = '<li><a href="/ja/themes/145">ベネッセグループのESG活動</a></li>'+
					  '<li><a href="/ja/themes/161">ガバナンス</a></li>'+
					  '<li id="active">税務基本方針</li>';
			} else if (themesId == 166) {
				elm = '<li id="active">サステナビリティデータ</li>';
			} else if (themesId == 167 || themesId == 124) {
				elm = '<li><a href="/ja/themes/166">サステナビリティデータ</a></li>'+
					  '<li id="active">ベネッセグループの概要</li>';
			} else if (themesId == 168 || themesId == 122) {
				elm = '<li><a href="/ja/themes/166">サステナビリティデータ</a></li>'+
					  '<li id="active">ESGデータ集</li>';
			} else if (themesId == 169 || themesId == 127) {
				elm = '<li><a href="/ja/themes/166">サステナビリティデータ</a></li>'+
					  '<li id="active">GRIスタンダード対照表</li>';
			} else if (themesId == 170 || themesId == 125) {
				elm = '<li><a href="/ja/themes/166">サステナビリティデータ</a></li>'+
					  '<li id="active">国連グローバル・コンパクト対照表</li>';
			} else if (themesId == 171 || themesId == 120) {
				elm = '<li><a href="/ja/themes/166">サステナビリティデータ</a></li>'+
					  '<li id="active">統合報告書</li>';
			} else if (themesId == 172 || themesId == 121) {
				elm = '<li><a href="/ja/themes/166">サステナビリティデータ</a></li>'+
					  '<li id="active">サステナビリティリンク集</li>';
			}

			if (themesId === undefined) {
				breadcrumb = '<div id="pan" class="scroll_In">'+
							 '<ul>'+
								'<li id="panHome"><a href="https://www.benesse-hd.co.jp/ja/index.html">トップページ</a></li>'+
								'<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/index.html">サステナビリティ</a></li>'+
								'<li id="active">サステナビリティデータ</li>'+
							 '</ul>'+
						 '</div>';
			} else {
				breadcrumb = '<div id="pan" class="scroll_In">'+
							 '<ul>'+
								'<li id="panHome"><a href="https://www.benesse-hd.co.jp/ja/index.html">トップページ</a></li>'+
								'<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/index.html">サステナビリティ</a></li>'+
								elm +
							 '</ul>'+
						 '</div>';
			}

			

		} else {
			if (themesId == 141 || themesId == 96) {
				elm = '<li><a href="https://www.benesse-hd.co.jp/en/sustainability/management/index.html">Sustainability Management</a></li>'+
					  '<li id="active">Communication with Stakeholders</li>';
			} else if (themesId == 142) {
				elm = '<li><a href="https://www.benesse-hd.co.jp/en/sustainability/management/index.html">Sustainability Management</a></li>'+
					  '<li id="active">Participation in Initiatives</li>';
			} else if (themesId == 145) {
				elm = '<li id="active">Benesse Group\'s ESG Initiatives</li>';
			} else if (themesId == 146 || themesId == 111) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li id="active">Environment</li>';
			} else if (themesId == 147 || themesId == 112) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/146">Environment</a></li>'+
					  '<li id="active">Environment Management</li>';
			} else if (themesId == 148 || themesId == 113) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/146">Environment</a></li>'+
					  '<li id="active">Our Response to Climate Change</li>';
			} else if (themesId == 149 || themesId == 113) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/146">Environment</a></li>'+
					  '<li id="active">Resource Conservation</li>';
			} else if (themesId == 150 || themesId == 114) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/146">Environment</a></li>'+
					  '<li id="active">Environmental Education</li>';
			} else if (themesId == 151 || themesId == 107) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li id="active">Society</li>';
			} else if (themesId == 152 || themesId == 129) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/151">Society</a></li>'+
					  '<li id="active">Respect for Human Rights</li>';
			} else if (themesId == 153 || themesId == 108) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/151">Society</a></li>'+
					  '<li id="active">Diversity and Inclusion</li>';
			} else if (themesId == 154 || themesId == 110) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/151">Society</a></li>'+
					  '<li id="active">Creating a Vibrant and Pleasant Workplace</li>';
			} else if (themesId == 155 || themesId == 109) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/151">Society</a></li>'+
					  '<li id="active">Human Resources Development</li>';
			} else if (themesId == 156) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/151">Society</a></li>'+
					  '<li id="active">Workplace Safety and Hygiene</li>';
			} else if (themesId == 157 || themesId == 115) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/151">Society</a></li>'+
					  '<li id="active">Local Communities</li>';
			} else if (themesId == 158) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/151">Society</a></li>'+
					  '<li id="active">品質・安全性の確保</li>';
			} else if (themesId == 159) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/151">Society</a></li>'+
					  '<li id="active">サプライチェーン・マネジメント</li>';
			} else if (themesId == 160) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/151">Society</a></li>'+
					  '<li id="active">Information Security</li>';
			} else if (themesId == 161) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li id="active">Governance</li>';
			} else if (themesId == 162) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/161">Governance</a></li>'+
					  '<li id="active">コーポレートガバナンス</li>';
			} else if (themesId == 163) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/161">Governance</a></li>'+
					  '<li id="active">内部統制</li>';
			} else if (themesId == 164) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/161">Governance</a></li>'+
					  '<li id="active">Risk Management and Compliance</li>';
			} else if (themesId == 165) {
				elm = '<li><a href="/en/themes/145">Benesse Group\'s ESG Initiatives</a></li>'+
					  '<li><a href="/en/themes/161">Governance</a></li>'+
					  '<li id="active">税務基本方針</li>';
			} else if (themesId == 166) {
				elm = '<li id="active">Sustainability Data</li>';
			} else if (themesId == 167 || themesId == 124) {
				elm = '<li><a href="/en/themes/166">Sustainability Data</a></li>'+
					  '<li id="active">Overview of Benesse Holdings, inc.</li>';
			} else if (themesId == 168 || themesId == 122) {
				elm = '<li><a href="/en/themes/166">Sustainability Data</a></li>'+
					  '<li id="active">ESG Data</li>';
			} else if (themesId == 169 || themesId == 127) {
				elm = '<li><a href="/en/themes/166">Sustainability Data</a></li>'+
					  '<li id="active">GRI Standards Content Index</li>';
			} else if (themesId == 170 || themesId == 125) {
				elm = '<li><a href="/en/themes/166">Sustainability Data</a></li>'+
					  '<li id="active">The United Nations\' Global Compact</li>';
			} else if (themesId == 171 || themesId == 120) {
				elm = '<li><a href="/en/themes/166">Sustainability Data</a></li>'+
					  '<li id="active">Integrated Report</li>';
			} else if (themesId == 172 || themesId == 121) {
				elm = '<li><a href="/en/themes/166">Sustainability Data</a></li>'+
					  '<li id="active">All Sustainability Links</li>';
			}


			if (themesId === undefined) {
				breadcrumb = '<div id="pan" class="scroll_In">'+
							 '<ul>'+
								'<li id="panHome"><a href="https://www.benesse-hd.co.jp/en/index.html">TOP</a></li>'+
								'<li><a href="https://www.benesse-hd.co.jp/en/sustainability/index.html">Sustainability</a></li>'+
								'<li id="active">Sustainability Data</li>'+
							 '</ul>'+
						 '</div>';
			} else {
				breadcrumb = '<div id="pan" class="scroll_In">'+
							 '<ul>'+
								'<li id="panHome"><a href="https://www.benesse-hd.co.jp/en/index.html">TOP</a></li>'+
								'<li><a href="https://www.benesse-hd.co.jp/en/sustainability/index.html">Sustainability</a></li>'+
								elm +
							 '</ul>'+
						 '</div>';
			}

			
		}
		/*
		var titletxt = $.map( $('title').text().split('|'), function(val,index){ return $.trim(val)} );

		var $pan = $('<div id="pan"><ul><li id="panHome"><a href="https://www.benesse-hd.co.jp/' + html_lang + '/index.html">' + (japaneseFlg?'トップページ':'TOP') + '</a></li><li><a href="https://www.benesse-hd.co.jp/' + html_lang + '/sustainability/index.html">' + (japaneseFlg?'サステナビリティ':'Sustainability') + '</a></li><li id="active">' + titletxt[0] + '</li></ul></div>');

		if( sustainaData ){
			$pan.find('#active').before( $('<li><a href="/' + html_lang + '">' + (japaneseFlg?'サステナビリティデータ':'Sustainability Data') + '</a></li>') );
		}
		if( sustainaAchievements ){
			$pan.find('#active').before( $('<li><a href="/' + html_lang + '#sustainability-achievements">' + (japaneseFlg?'ベネッセグループの取り組み':'Sustainability Achievements') + '</a></li>') );
		}
		var parentId =
		(themesId>=99 && themesId<=102)?98
		:(themesId>=105 && themesId<=106)?104
		:(themesId>=108 && themesId<=110)?107
		:(themesId>=112 && themesId<=114)?111
		:(themesId>=116 && themesId<=117)?115:0;
		if( parentId ){
			$pan.find('#active').before( $('<li><a href="/' + html_lang + '/themes/' + parentId + '">' + titletxt[1] + '</a></li>') );
		}
		*/
		/* 取り組み以外で親子関係がある場合ヘッダーフッターメニュー処理後
		if( titletxt[0] !== titletxt[1] ){
			$('#sideMenu a').each(function(){
				if( $(this).text() ==  titletxt[1] ){
					$pan.find('#active').before( $(this).parent().clone() );
					return false;
				}
			});
		}*/


		$('#footer').before($('#LastUpDate'));

		$('#footer').before(breadcrumb);
		//$('#footer').before($pan);

		$('.Header, .ThemeItem-company, .Enquetes, .home-ctrl, .Pagination, .ThemeHead-main, .ThemeItem-images').remove();

		$('.Topics-items, .Topics>.Topics-contents').addClass('contents');// .ThemeHeadには付けない方向で

		$('.ThemeItem-title').addClass('titleLv2');

		//不要なカード削除 記事に<del class="all_article"><!-- このセクションを親要素ごと削除します --></del>記載
		$('.all_article').closest('.ThemeItem').remove();

		//$('.ThemeItem').each(function(){
		//	$(this).addClass( 'sustaina-' + $(this).attr('id') );
		//});


		$html = $('html');
		$body = $('body');

		$(window).on('load',function(){
			if(location.hash){
				if($(location.hash).length>0){
					$html.add($body).animate({ scrollTop: $(location.hash).offset().top - 75 },500,'swing');
				}
			}

			$html.addClass('visible');

			// FADE
			$('#wrapper').addClass('pageLoaded');

			// fade_In CLASS
			var view_bottom_up = $(window).scrollTop() + $(window).height() + Math.max(40-0.033*$(window).width(),0) - 75;
			$('.ThemeItem, .boxLnks').each(function(){
				if( $(this).offset().top > view_bottom_up ){
					$(this).addClass('fade_In');
				}
			});

		})
		// HEADER SLIDE
		.on('load scroll',function(){
			if($(this).scrollTop()>40){
				$('#wrapper').addClass('up');
			}else{
				$('#wrapper').removeClass('up');
			}
		});


		//
		// SETTING NAVIGATION
		//
		$('#gnav .gnav a').append('<svg xmlns="http://www.w3.org/2000/svg" width="9.8" height="7" viewBox="0 0 9.8 7"><path d="M0,1.4A1.41,1.41,0,0,1,.5.32a1.42,1.42,0,0,1,2,.18L4.9,3.41,7.32.5a1.42,1.42,0,0,1,2-.18,1.42,1.42,0,0,1,.18,2L6,6.5a1.42,1.42,0,0,1-2.16,0L.32,2.3A1.46,1.46,0,0,1,0,1.4Z"/></svg>');

		// サムネイル
		$('#cnavWrap .cnavThb a:not([target]) em').prepend('<svg xmlns="http://www.w3.org/2000/svg" width="7" height="9.8" viewBox="0 0 7 9.8"><path d="M1.4,9.8A1.41,1.41,0,0,1,.32,9.3a1.42,1.42,0,0,1,.18-2L3.41,4.9.5,2.48A1.42,1.42,0,0,1,.32.5a1.42,1.42,0,0,1,2-.18l4.2,3.5A1.42,1.42,0,0,1,6.5,6L2.3,9.48A1.46,1.46,0,0,1,1.4,9.8Z"/></svg>');

		$('#cnavWrap .cnavTxt li div').each(function(){
			$(this).prev('strong').children('a').addClass('subParent');
		});


		var $menuWrap = $('#menuWrap');
		$menuWrap.children('ul:eq(0)').children('li').remove();

		var $copyCnav = $('#cnavWrap .cnavWrap');
		$('#gnav .gnav a').each(function(index){
			var $copyMenu = $copyCnav.eq(index).clone();

			$copyMenu.prepend($(this).clone()).find('.cnavTop').find('strong').remove()//.end().children('ul').appendTo($copyMenu.find('.cnavTxt').children('div'));

			$menuWrap.children('ul:eq(0)').append($copyMenu);
		});

		// メインタイトル
		$('#cnavWrap .cnavTop em').append('<span><svg xmlns="http://www.w3.org/2000/svg" width="7" height="9.8" viewBox="0 0 7 9.8"><path d="M1.4,9.8A1.41,1.41,0,0,1,.32,9.3a1.42,1.42,0,0,1,.18-2L3.41,4.9.5,2.48A1.42,1.42,0,0,1,.32.5a1.42,1.42,0,0,1,2-.18l4.2,3.5A1.42,1.42,0,0,1,6.5,6L2.3,9.48A1.46,1.46,0,0,1,1.4,9.8Z"/></svg></span>');

		$('#cnavWrap .cnavTxt strong>a').each(function(){
			if($(this).closest('li').children('div').length>0){
				$(this).append('<svg xmlns="http://www.w3.org/2000/svg" width="11" height="11" viewBox="0 0 11 11"><line x1="1.5" y1="5.5" x2="9.5" y2="5.5"/><line class="a" x1="5.5" y1="9.5" x2="5.5" y2="1.5"/></svg>');
			}else{
				$(this).append('<svg xmlns="http://www.w3.org/2000/svg" width="7" height="9.8" viewBox="0 0 7 9.8"><path d="M1.4,9.8A1.41,1.41,0,0,1,.32,9.3a1.42,1.42,0,0,1,.18-2L3.41,4.9.5,2.48A1.42,1.42,0,0,1,.32.5a1.42,1.42,0,0,1,2-.18l4.2,3.5A1.42,1.42,0,0,1,6.5,6L2.3,9.48A1.46,1.46,0,0,1,1.4,9.8Z"/></svg>');
			}
		});

		$('#gnav a, #cnavWrap .cnavTxt strong a, #menuWrap .cnavWrap>a, #head>a').each(function(){
			$(this).on({
				'mouseenter': function(){ $(this).addClass('hov');},
				'mouseleave': function(){ $(this).removeClass('hov');}
			});
		});


		//
		// PC GLOBAL NAVIGATION
		//
		$gnavDropMunu = $('#cnavWrap .cnavWrap').on('click',function(e){
			e.stopPropagation();
		});

		// 閉じる
		$gnavDropMunu.append( $('<a class="x_Close"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><circle cx="20" cy="20" r="18.5"/><path d="M20,3A17,17,0,1,1,3,20,17,17,0,0,1,20,3m0-3A20,20,0,1,0,40,20,20,20,0,0,0,20,0Z"/><line x1="25.14" y1="15.09" x2="14.86" y2="24.91"/><line x1="15.09" y1="14.86" x2="24.91" y2="25.14"/></svg><span>' + (japaneseFlg?'閉じる':'Close') + '</span></a>') );

		$(window).add( $gnavDropMunu.find('.x_Close') ).on('click',function(){
			if($gnavDropMunu.filter('.openDrop').length>0){
				$('#gnav a.act').trigger('click');
			}
		});

		$gnavDropButton = $('#gnav a').each(function(index){
			if(index!=4){
				$(this).on('click',function(){
					$gnavDropButton.removeClass('hov').not( $(this).toggleClass('act') ).removeClass('act');

					if($(this).hasClass('act')){

						$gnavDropMunu.eq(index)
						.find('.cnavThb').show()
						.end().find('.subPopen').removeClass('subPopen hov').closest('li').children('div').hide();

						$gnavDropMunu.eq(index).scrollTop(0).addClass('openDrop').siblings('li').removeClass('openDrop');

						if($('#wrapper').hasClass('up')){
							$('#wrapper').addClass('upDrop');
						}

						bodyFixation();
					}else{
						$gnavDropMunu.eq(index).removeClass('openDrop');

						if($('#wrapper').hasClass('upDrop')){
							$('#wrapper').removeClass('upDrop').addClass('up');
						}

						bodyRelease();
					}

					return false;
				});
			}
		});

		var $cnavWrapSubParent = $('#cnavWrap .cnavWrap').each(function(index){

			$(this).find('.subParent').on('click',function(){

				$(this).addClass('subPopen').removeClass('hov').closest('li').children('div').fadeIn(600);

				$cnavWrapSubParent.eq(index).find('.cnavThb').fadeOut(600)

				.end().find('.subParent').not($(this)).removeClass('subPopen').closest('li').children('div').fadeOut(600);

				return false;
			});
		});


		//
		// SP TOGGLE NAVIGATION
		//
		$('#menu').on('click', function(){
			$(this).removeClass('hov');

			$html.toggleClass('menuOpen');

			if($html.hasClass('menuOpen')){


				$html.removeClass('searchOpen');


				bodyFixation();
			}else{
				$('#menuWrap .cnav').slideUp(0).prev('a').removeClass('cnavOpen');

				$menuWrapSubParent.removeClass('csubOpen').closest('li').children('div').hide();

				bodyRelease();
			}
			return false;
		});

		$('#menuWrap .cnav').each(function(index){
			$(this).prev('a').on('click',function(){
				$('#menuWrap .cnavWrap>a').removeClass('hov')

				$(this).toggleClass('cnavOpen').next('.cnav').slideToggle(600).parent('.cnavWrap').siblings('li').children('a').removeClass('cnavOpen').next('.cnav').slideUp(600);

				if($(this).hasClass('cnavOpen')){
					$menuWrap.animate({ scrollTop:(index*60+20) },600,'swing');
				}else{
					$menuWrap.animate({ scrollTop:0 },600,'swing');
				}

				return false;
			});
		});
		$menuWrapSubParent = $('#menuWrap .subParent').each(function(){
			$(this).on('click',function(){

				$(this).toggleClass('csubOpen').closest('li').children('div').slideToggle(600)
				.parent('li').siblings('li').find('.subParent').removeClass('csubOpen').closest('li').children('div').slideUp(600);

				return false;
			});
		});


		$('#ss_menu').on('click', function(){
			$(this).removeClass('hov');

			$html.toggleClass('searchOpen');

			if($html.hasClass('searchOpen')){

				$(this).addClass('act');
				$html.removeClass('menuOpen');

				bodyFixation();
			}else{
				$(this).removeClass('act');

				bodyRelease();
			}
			return false;
		});

		$('#searchWrap2 .x_Close').on('click',function(){
			$('#ss_menu').trigger('click');
		});

		// SEARCH
		var currentResizeFlag = $('#logo').css('font-size');

		$(window).on('load',function(){
			currentResizeFlag = $('#logo').css('font-size');

			$('#ss-category-field-default').addClass('searchCategoryCommonClass').find('input, label').wrapAll('<div class="flex_bc"></div>');
			$('#ss-category-field-file_type').addClass('searchFiletypeCommonClass').find('input, label').wrapAll('<div class="flex_cc"></div>');
		});

		$(window).on('resize',function(){

			var sppcFlag = $('#logo').css('font-size');

			if(sppcFlag=='20px' && currentResizeFlag=='10px'){
				$('html.menuOpen  #menu, html.searchOpen #ss_menu').trigger('click');

			}else if(sppcFlag=='10px' && currentResizeFlag=='20px'){
				$('#gnav a.act').trigger('click');

			}
			currentResizeFlag = sppcFlag;
		});


		//
		// DROP % MENU OPEN BODY
		//
		var currentscroll = $(window).scrollTop();

		function bodyFixation(){
			currentscroll = $(window).scrollTop();

			$html.addClass('bodyFix');
			$body.css('top',-currentscroll);
		}

		function bodyRelease(){
			$html.removeClass('bodyFix');
			$body.css('top',0);
			$(window).scrollTop(currentscroll);
		}


		// 画像リミット（削除予定）
		$('#contents img[width].full').each(function(){
			var width = $(this).attr('width');
			if(!isNaN(width)){
				$(this).css('max-width', width+'px');
			}
		});

		// 印刷ボタン
		$('<div />').addClass('prn').appendTo('#wrap').on('click',function(){
			window.print();
			return false;
		});


		// ページのトップへ
		$('#pan').append('<div id="ptop"><a href="#wrapper" class="scroll"><span></span><i class="exb">PAGE TOP</i></a></div>');
		$('#ptop span').append($('#gnav .gnav:eq(0) svg').clone());


		// SMOOTH SCROLL
		$('a.scroll').on('click', function(){
			var href= $(this).attr('href');
			$html.add($body).animate({ scrollTop:(href=='#')?0:( $(href).offset().top - $('#header').height() -5 ) },500,'swing');
			return false;
		});


		// SCROLL EFFECT
		$(window).on('load resize scroll',function(event){

			var view_bottom_up = $(window).scrollTop() + $(window).height() + Math.max(40-0.033*$(window).width(),0) - 150;

			if(event.type=='load'){
				view_bottom_up = view_bottom_up + 75;
			}

			$('.fade_In:not(.scroll_In), #pan:not(.scroll_In), #LastUpDate:not(.scroll_In), #footer:not(.scroll_In)').each(function(){
				if( $(this).offset().top < view_bottom_up ){
					$(this).addClass('scroll_In');
				}
			});

			$('.scroll_Obs:not(.scroll_In)').each(function(index){
				if( $(this).offset().top < (view_bottom_up - ($(this).attr('data-above')||0)) ){
					$(this).addClass('scroll_In');
				}
			});
		});
	});


	// Font Size
	function fontSize(){

		var $fontSize = $('#fsize');

		if(localStorage.getItem('benesseFont')=='L'){
			$('html').addClass('largeBase');
			$fontSize.addClass('Large');
		}

		$fontSize.find('a').each(function(index){
			$(this).on('click',function(){
				if(index==0){
					$('html').removeClass('largeBase');
					$fontSize.removeClass('Large');
					var size = 'N';
				}else{
					$('html').addClass('largeBase');
					$fontSize.addClass('Large');
					var size = 'L';
				}
				try{ localStorage.setItem('benesseFont',size);}catch(e){};
			});
		});
	}


//'
var headerInc_ja = function(){/*
<header id="header">
	<div id="head">
		<div id="logo"><a href="https://www.benesse-hd.co.jp/ja/index.html"><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/logo.svg" alt="ベネッセホールディングス"></a></div>
		<ul class="unav">
			<li><a href="https://www.benesse.co.jp/" target="_blank" class="Lar over"><span>ベネッセグループの商品・サービス</span></a></li>
			<li><a href="/en/index.html" id="lang" class="Lar over"><span>English</span></a></li>
			<li>
				<dl id="fsize"><dt>文字サイズ</dt><dd><a id="fsizeN">標準</a><a id="fsizeL">拡大</a></dd></dl>
			</li>
		</ul>
		<a id="menu"><span id="menubar"></span></a>
		<a id="ss_menu" class="ss_menu"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><circle cx="20" cy="20" r="18.5"/><path class="a" d="M20,3A17,17,0,1,1,3,20,17,17,0,0,1,20,3m0-3A20,20,0,1,0,40,20,20,20,0,0,0,20,0Z"/><path class="b" d="M18.26,14.22a4,4,0,1,1-2.78,1.11,4,4,0,0,1,2.78-1.11m0-3a7,7,0,1,0,5.12,2.21,7,7,0,0,0-5.12-2.21Z"/><line x1="22.89" y1="23.1" x2="26.71" y2="27.39"/></svg></a>
	</div>
	<ul id="gnav">
		<li class="gnav"><a href="https://www.benesse-hd.co.jp/ja/about/index.html"><span>企業・グループ情報</span></a></li>
		<li class="gnav"><a href="https://blog.benesse.ne.jp/bh/ja/news/archives.html"><span>お知らせ</span></a></li>
		<li class="gnav"><a href="https://www.benesse-hd.co.jp/ja/ir/index.html"><span>投資家(IR)情報</span></a></li>
		<li class="gnav"><a href="https://www.benesse-hd.co.jp/ja/sustainability/index.html"><span>サステナビリティ</span></a></li>
		<li class="gnav"><a href="https://www.benesse-hd.co.jp/ja/recruit/index.html"><span>採用情報</span></a></li>
		<li class="snav"><a href="#" class="ss_menu"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><circle cx="20" cy="20" r="18.5"/><path class="a" d="M20,3A17,17,0,1,1,3,20,17,17,0,0,1,20,3m0-3A20,20,0,1,0,40,20,20,20,0,0,0,20,0Z"/><path class="b" d="M18.26,14.22a4,4,0,1,1-2.78,1.11,4,4,0,0,1,2.78-1.11m0-3a7,7,0,1,0,5.12,2.21,7,7,0,0,0-5.12-2.21Z"/><line x1="22.89" y1="23.1" x2="26.71" y2="27.39"/></svg></a></li>
	</ul>
	<ul id="cnavWrap">
		<li class="cnavWrap">
			<ul class="cnav">
				<li class="cnavTop">
					<a href="https://www.benesse-hd.co.jp/ja/about/index.html"><strong class="exb">ABOUT BENESSE</strong><em>企業・グループ情報トップ</em></a>
				</li>
				<li class="cnavTxt">
					<ul>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/about/message.html"><span>トップメッセージ</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/about/philosophy.html"><span>グループ企業理念・行動指針</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/about/management/business.html"><span>事業内容</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/about/management/governance.html"><span>ガバナンス</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/about/management/governance.html"><b>ガバナンス トップ</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/ja/about/management/corp_governance.html">コーポレート・ガバナンス</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/about/management/internal_control.html">内部統制システムに関する基本的な考え方及びその整備状況</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/about/management/antisocial.html">反社会的勢力排除に関する基本方針</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/about/management/taxpolicy.html">ベネッセグループ税務基本方針</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/about/corporate.html"><span>会社情報</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/about/corporate.html"><b>会社情報 トップ</b></a>
										<div>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/ja/about/company.html">会社概要</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/about/management/executive.html">役員一覧</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/about/group.html">グループ会社</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/about/organization.html">組織図</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/about/history.html">グループ沿革</a></li>
											</ul>
											<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/ja/about/profile.html">ベネッセグループ会社案内</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/about/channel.html">動画で見るベネッセ</a></li>
											<li><a href="https://www.benesse.co.jp/brand/" target="_blank">サステナビリティ活動について</a><img class="blank" src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/icon/icon_blank.png" width="15" height="15" alt=""></li>
											<li><a href="https://www.benesse.co.jp/digital/" target="_blank">ベネッセグループのDXについて</a><img class="blank" src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/icon/icon_blank.png" width="15" height="15" alt=""></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/b-stage" target="_blank">提案制度「B-STAGE」について</a><img class="blank" src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/icon/icon_blank.png" width="15" height="15" alt=""></li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/about/naoshima.html"><span>ベネッセアートサイト直島</span></a></strong></li>
					</ul>
				</li>
				<li class="cnavThb">
					<ul>
						<li><a href="https://www.benesse-hd.co.jp/ja/about/profile.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana014.jpg" width="300" height="180" alt=""></span><em>ベネッセグループ会社案内</em></a></li>
						<li><a href="https://www.benesse-hd.co.jp/ja/about/philosophy.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana012.jpg" width="170" height="180" alt=""></span><em>企業理念・行動指針</em></a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="cnavWrap">
			<ul class="cnav">
				<li class="cnavTop">
					<a href="https://blog.benesse.ne.jp/bh/ja/news/archives.html"><strong class="exb">NEWS</strong><em>お知らせ</em></a>
				</li>
				<li class="cnavTxt">
					<ul class="bcNewsControl">
						<li><strong><a href="https://blog.benesse.ne.jp/bh/ja/news/archives.html"><span>ニュースリリース</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://blog.benesse.ne.jp/bh/ja/news/archives.html">一覧</a>
										<ul class="cnavSub2">
											<li><a href="https://blog.benesse.ne.jp/bh/ja/news/2021/">2021年</a></li>
											<li><a href="https://blog.benesse.ne.jp/bh/ja/news/2020/">2020年</a></li>
											<li><a href="https://blog.benesse.ne.jp/bh/ja/news/2019/">2019年</a></li>
											<li><a href="https://blog.benesse.ne.jp/bh/ja/news/2018/">2018年</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/ir/ir_news/index.html"><span>IRニュース</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/ir/ir_news/index.html"><b>最新1年分</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/ir_news/past.html?year=2021">2021年</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/ir_news/past.html?year=2020">2020年</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/ir_news/past.html?year=2019">2019年</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/ir_news/past.html?year=2018">2018年</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://blog.benesse.ne.jp/bh/ja/csr_news/info/"><span>サステナビリティニュース</span></a></strong></li>
					</ul>
				</li>
				<li class="cnavThb">
					<ul>
						<li></li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="cnavWrap">
			<ul class="cnav">
				<li class="cnavTop">
					<a href="https://www.benesse-hd.co.jp/ja/ir/index.html"><strong class="exb">INVESTOR RELATIONS</strong><em>投資家(IR)情報トップ</em></a>
				</li>
				<li class="cnavTxt">
					<ul>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/ir/strategy/index.html"><span>経営方針</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/ir/strategy/index.html"><b>経営方針 トップ</b></a>
										<div>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/ja/about/message.html">トップメッセージ</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/strategy/middleplan.html">中期経営計画</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/about/management/governance.html">ガバナンス</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/strategy/communication.html">株主・投資家とのコミュニケーション</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/strategy/risk.html">事業等のリスク</a></li>
											</ul>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/strategy/ir_policy.html">IR基本方針</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/strategy/award.html">受賞歴</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/strategy/creation_model.html">ベネッセの価値創造モデル</a></li>
												<li><a href="https://www.benesse.co.jp/digital/" target="_blank">ベネッセグループのDXについて</a><img class="blank" src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/icon/icon_blank.png" width="15" height="15" alt=""></li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/ir/finance/index.html"><span>財務・業績情報</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/ir/finance/index.html"><b>財務・業績情報 トップ</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/finance/chart.html">財務パフォーマンス</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/finance/data_book.html">DATA BOOK</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/finance/forecast.html">業績予想</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/finance/subsidiary.html">主要子会社及び関連会社</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/ir/library/index.html"><span>IR資料</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/ir/library/index.html"><b>IR資料 トップ</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/library/results/index.html">決算短信・説明会資料</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/library/ar/index.html">統合報告書</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/library/factsheet/index.html">ファクトシート</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/library/yuho/index.html">有価証券報告書</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/library/kabutsu/index.html">株主通信</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/ir/stock/index.html"><span>株式情報</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/index.html"><b>株式情報 トップ</b></a>
										<div>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/chart.html">株価情報</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/basic.html">株式基本情報</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/return.html">株主還元</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/shareholders_meeting.html">株主総会</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/dividend.html">配当実績</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/split.html">株式分割実施状況</a></li>
											</ul>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/repurchase.html">自己株式の取得・消却状況</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/bond.html">社債・格付情報</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/analyst.html">アナリストカバレッジ</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/contact.html">株式手続きのご案内</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/denshika.html">株券電子化実施に伴うお知らせ</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/stock/shinsai.html">自然災害により被災された株主様へ</a></li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/ir/yutai/index.html"><span>株主優待</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/ir/yutai/index.html"><b>株主優待制度について</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/yutai/present.html">2021年3月期株主優待品</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/yutai/faq.html">よくあるご質問</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/ir/individual/index.html"><span>個人投資家の皆様へ</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/ir/individual/index.html"><b>個人投資家の皆様へ トップ</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/individual/beginner.html">はじめてのベネッセグループ</a></li>
											<li><a href="https://www.benesse-hd.co.jp/ja/ir/individual/event.html">個人投資家向けイベント</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://blog.benesse.ne.jp/bh/ja/ir_calendar/2020/"><span>IRカレンダー</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/ir/support/index.html"><span>サポート</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/ir/support/index.html"><b>サポート トップ</b></a>
										<div>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/ja/ir/ir_mail/index.html">IRメール配信</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/utility/faq.html">よくあるご質問</a></li>
												<li><a href="https://enquete.benesse.ne.jp/forms/o/we9f8d8568/form.php" target="_blank">お問い合わせ</a><img class="blank" src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/icon/icon_blank.png" width="15" height="15" alt=""></li>
											</ul>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/ja/utility/recommend.html">サイト内おすすめルート</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/utility/glossary.html">用語集</a></li>
												<li><a href="https://www.benesse-hd.co.jp/ja/utility/clause.html">免責事項</a></li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</li>
				<li class="cnavThb">
					<ul>
						<li><a href="https://www.benesse-hd.co.jp/ja/about/message.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana021_2021.jpg" width="170" height="180" alt=""></span><em>トップメッセージ</em></a></li>
						<li><a href="https://www.benesse-hd.co.jp/ja/ir/strategy/middleplan.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana024.jpg" width="170" height="180" alt=""></span><em>新中期経営計画</em></a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="cnavWrap">
			<ul class="cnav">
				<li class="cnavTop">
					<a href="https://www.benesse-hd.co.jp/ja/sustainability/index.html"><strong class="exb">SUSTAINABILITY</strong><em>サステナビリティトップ</em></a>
				</li>
				<li class="cnavTxt">
					<ul>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/sustainability/message/index.html"><span>トップメッセージ</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/sustainability/vision/index.html"><span>サステナビリティビジョン</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/sustainability/management/index.html"><span>サステナビリティマネジメント</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/management/index.html"><b>サステナビリティマネジメント</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/framework/index.html"><span>サステナビリティ推進体制</span></a></li>
											<li><a href="/ja/themes/141">ステークホルダーとのコミュニケーション</a></li>
											<li><a href="/ja/themes/142">イニシアティブへの参加</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/sustainability/materiality/index.html"><span>ベネッセのマテリアリティ</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/materiality/index.html"><b>ベネッセのマテリアリティ</b></a>
										<ul class="cnavSub2">
											<li><a href="https://blog.benesse.ne.jp/bh/ja/sustainability/activity/index.html">マテリアリティ活動事例</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="/ja/themes/145"><span>ベネッセグループのESG活動</span></a></strong>
							<div>
								<ul class="cnavSub">
										<li><a href="/ja/themes/145"><b>ベネッセグループのESG活動</b></a>
										<ul class="cnavSub2">
											<li><a href="/ja/themes/146">環境</a></li>
											<li><a href="/ja/themes/151">社会</a></li>
											<li><a href="/ja/themes/161">ガバナンス</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="/ja/themes/166"><span>サステナビリティデータ</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="/ja/themes/166"><strong>サステナビリティデータ</strong></a>
										<div>
											<ul class="cnavSub2">
												<li><a href="/ja/themes/167">ベネッセグループの概要</a></li>
												<li><a href="/ja/themes/168">ESGデータ集</a></li>
												<li><a href="/ja/themes/169">GRIスタンダード対照表</a></li>
											</ul>
											<ul class="cnavSub2">
												<li><a href="/ja/themes/170">国連グローバル・コンパクト</a></li>
												<li><a href="/ja/themes/171">統合報告書</a></li>
												<li><a href="/ja/themes/172">サステナビリティリンク集</a></li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/sustainability/evaluation/index.html"><span>外部評価・受賞実績</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/ja/sustainability/mirai/index.html" target="_blank"><span>こどもの未来応援プロジェクト</span></a></strong></li>
					</ul>
				</li>
				<li class="cnavThb">
					<ul>
						<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/vision/index.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana032.jpg" width="170" height="180" alt=""></span><em>サステナビリティビジョン</em></a></li>
						<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/materiality/index.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana033.jpg" width="170" height="180" alt=""></span><em>ベネッセのマテリアリティ</em></a></li>
						<li><a href="/ja/themes/145"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana034.jpg" width="170" height="180" alt=""></span><em>ベネッセグループの<br>ESG活動</em></a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="cnavWrap"></li>
		<li class="cnavWrap searchWrap" id="searchWrap1">

			<form class="searchFormCommonClass" id="ss-form1" action="https://www.benesse-hd.co.jp/ja/search/" method="get" accept-charset="UTF-8">
				<input type="text" id="ss-query1" name="query" autocomplete="off" aria-autocomplete="none" placeholder="キーワードを入力" class="searchInputCommonClass">
				<input type="hidden" name="charset" value="utf-8">

				<div class="ss-categories">
					<fieldset class="searchCategoryCommonClass ss-category-field">
						<div class="flex_bc">
							<input type="checkbox" name="category" value="1" class="ss-category" id="ss-category-default0-pc">
							<label for="ss-category-default0-pc">企業・グループ情報</label>
							<input type="checkbox" name="category" value="2" class="ss-category" id="ss-category-default1-pc">
							<label for="ss-category-default1-pc">お知らせ</label>
							<input type="checkbox" name="category" value="3" class="ss-category" id="ss-category-default2-pc">
							<label for="ss-category-default2-pc">投資家<br>(IR)情報</label>
							<input type="checkbox" name="category" value="4" class="ss-category" id="ss-category-default3-pc">
							<label for="ss-category-default3-pc">サステナ<br>ビリティ</label>
							<input type="checkbox" name="category" value="5" class="ss-category" id="ss-category-default4-pc">
							<label for="ss-category-default4-pc">採用情報</label>
						</div>
					</fieldset>
				</div>

				<fieldset class="searchFiletypeCommonClass ss-category-field">
					<div class="flex_cc">
						<input type="radio" name="file_type" value="1" checked="checked" class="ss-category" id="ss-category-file_type0-pc">
						<label for="ss-category-file_type0-pc">すべて</label>
						<input type="radio" name="file_type" value="2" class="ss-category" id="ss-category-file_type1-pc">
						<label for="ss-category-file_type1-pc">HTML</label>
						<input type="radio" name="file_type" value="3" class="ss-category" id="ss-category-file_type2-pc">
						<label for="ss-category-file_type2-pc">PDF</label>
					</div>
				</fieldset>

				<button type="submit" class="searchButtonCommonClass ss-search-button ss_menu flex_cc"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path class="b" d="M18.26,14.22a4,4,0,1,1-2.78,1.11,4,4,0,0,1,2.78-1.11m0-3a7,7,0,1,0,5.12,2.21,7,7,0,0,0-5.12-2.21Z"></path><line x1="22.89" y1="23.1" x2="26.71" y2="27.39"></line></svg><span>検索する</span></button>
			</form>
		</li>
	</ul>

	<nav id="menuWrap">
		<ul>
			<li></li>
		</ul>
		<ul class="unav">
			<li><a href="https://www.benesse.co.jp/" target="_blank" class="Lar over"><span>ベネッセグループの商品・サービス</span></a></li>
			<li><a href="/en/index.html" id="lang2" class="Lar over"><span>English</span></a></li>
		</ul>
	</nav>
	<div class="searchWrap" id="searchWrap2">

		<form class="searchFormCommonClass" id="ss-form2" action="https://www.benesse-hd.co.jp/ja/search/" method="get" accept-charset="UTF-8">
			<input type="text" id="ss-query2" name="query" autocomplete="off" aria-autocomplete="none" placeholder="キーワードを入力" class="searchInputCommonClass">
			<input type="hidden" name="charset" value="utf-8">

			<div class="ss-categories">
				<fieldset class="searchCategoryCommonClass ss-category-field">
					<div class="flex_bc">
						<input type="checkbox" name="category" value="1" class="ss-category" id="ss-category-default0-sp">
						<label for="ss-category-default0-sp">企業・グループ情報</label>
						<input type="checkbox" name="category" value="2" class="ss-category" id="ss-category-default1-sp">
						<label for="ss-category-default1-sp">お知らせ</label>
						<input type="checkbox" name="category" value="3" class="ss-category" id="ss-category-default2-sp">
						<label for="ss-category-default2-sp">投資家<br>(IR)情報</label>
						<input type="checkbox" name="category" value="4" class="ss-category" id="ss-category-default3-sp">
						<label for="ss-category-default3-sp">サステナ<br>ビリティ</label>
						<input type="checkbox" name="category" value="5" class="ss-category" id="ss-category-default4-sp">
						<label for="ss-category-default4-sp">採用情報</label>
					</div>
				</fieldset>
			</div>

			<fieldset class="searchFiletypeCommonClass ss-category-field">
				<div class="flex_cc">
					<input type="radio" name="file_type" value="1" checked="checked" class="ss-category" id="ss-category-file_type0-sp">
					<label for="ss-category-file_type0-sp">すべて</label>
					<input type="radio" name="file_type" value="2" class="ss-category" id="ss-category-file_type1-sp">
					<label for="ss-category-file_type1-sp">HTML</label>
					<input type="radio" name="file_type" value="3" class="ss-category" id="ss-category-file_type2-sp">
					<label for="ss-category-file_type2-sp">PDF</label>
				</div>
			</fieldset>

			<button type="submit" class="searchButtonCommonClass ss-search-button ss_menu flex_cc"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path class="b" d="M18.26,14.22a4,4,0,1,1-2.78,1.11,4,4,0,0,1,2.78-1.11m0-3a7,7,0,1,0,5.12,2.21,7,7,0,0,0-5.12-2.21Z"></path><line x1="22.89" y1="23.1" x2="26.71" y2="27.39"></line></svg><span>検索する</span></button>
		</form>
		<a class="x_Close"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><circle cx="20" cy="20" r="18.5"/><path d="M20,3A17,17,0,1,1,3,20,17,17,0,0,1,20,3m0-3A20,20,0,1,0,40,20,20,20,0,0,0,20,0Z"/><line x1="25.14" y1="15.09" x2="14.86" y2="24.91"/><line x1="15.09" y1="14.86" x2="24.91" y2="25.14"/></svg><span>閉じる</span></a>
	</div>
</header>
*/}.toString().match(/\/\*([^]*)\*\//)[1].replace(/\n|\r|\t/g,'');


var headerInc_en = function(){/*
<header id="header">
	<div id="head">
		<div id="logo"><a href="https://www.benesse-hd.co.jp/en/index.html"><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/en/hd-common/images/logo.svg" alt="Benesse Holdings ,Inc."></a></div>
		<ul class="unav">
			<li><a href="/ja/index.html" id="lang" class="Lar over"><span>Japanese</span></a></li>
			<li>
				<dl id="fsize"><dt>Text Size</dt><dd><a id="fsizeN">Default</a><a id="fsizeL">Large</a></dd></dl>
			</li>
		</ul>
		<a id="menu"><span id="menubar"></span></a>
		<a id="ss_menu" class="ss_menu"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><circle cx="20" cy="20" r="18.5"/><path class="a" d="M20,3A17,17,0,1,1,3,20,17,17,0,0,1,20,3m0-3A20,20,0,1,0,40,20,20,20,0,0,0,20,0Z"/><path class="b" d="M18.26,14.22a4,4,0,1,1-2.78,1.11,4,4,0,0,1,2.78-1.11m0-3a7,7,0,1,0,5.12,2.21,7,7,0,0,0-5.12-2.21Z"/><line x1="22.89" y1="23.1" x2="26.71" y2="27.39"/></svg></a>
	</div>
	<ul id="gnav">
		<li class="gnav"><a href="https://www.benesse-hd.co.jp/en/about/index.html"><span>About Benesse</span></a></li>
		<li class="gnav"><a href="https://www.benesse-hd.co.jp/en/ir/index.html"><span>Investor Relations</span></a></li>
		<li class="gnav"><a href="https://www.benesse-hd.co.jp/en/sustainability/index.html"><span>Sustainability</span></a></li>
		<li class="snav"><a href="#" class="ss_menu"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><circle cx="20" cy="20" r="18.5"/><path class="a" d="M20,3A17,17,0,1,1,3,20,17,17,0,0,1,20,3m0-3A20,20,0,1,0,40,20,20,20,0,0,0,20,0Z"/><path class="b" d="M18.26,14.22a4,4,0,1,1-2.78,1.11,4,4,0,0,1,2.78-1.11m0-3a7,7,0,1,0,5.12,2.21,7,7,0,0,0-5.12-2.21Z"/><line x1="22.89" y1="23.1" x2="26.71" y2="27.39"/></svg></a></li>
	</ul>
	<ul id="cnavWrap">
		<li class="cnavWrap">
			<ul class="cnav">
				<li class="cnavTop">
					<a href="https://www.benesse-hd.co.jp/en/about/index.html"><em class="exb">About Benesse</em></a>
				</li>
				<li class="cnavTxt">
					<ul>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/about/message.html"><span>Top Message</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/about/philosophy.html"><span>Benesse Group Corporate Philosophy and Principle</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/about/management/business.html"><span>Our Business</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/about/management/governance.html"><span>Governance</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/about/management/governance.html"><b>Governance</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/en/about/management/corp_governance.html">Corporate Governance</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/about/management/internal_control.html">Basic Stance and Implementation of Internal Control System</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/about/management/antisocial.html">Basic Stance and Measures to Eliminate Antisocial Forces</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/about/management/taxpolicy.html">Benesse Group Tax Policy</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/about/corporate.html"><span>Corporate and Group Information</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/about/corporate.html"><b>Corporate and Group Information</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/en/about/company.html">Corporate Data</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/about/management/executive.html">Management</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/about/group.html">Group Companies</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/about/history.html">Group History</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/about/profile.html">Benesse Group Corporate Guide</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/about/naoshima.html"><span>Benesse Art Site Naoshim</span></a></strong></li>
					</ul>
				</li>
				<li class="cnavThb">
					<ul>
						<li><a href="https://www.benesse-hd.co.jp/en/about/profile.html"><span class="border"><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/en/hd-common/images/nav/bana014.jpg" width="300" height="180" alt=""></span><em>Benesse Group Corporate Guide</em></a></li>
						<li><a href="https://www.benesse-hd.co.jp/en/about/philosophy.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana012.jpg" width="170" height="180" alt=""></span><em>Corporate Philosophy <br>and Principle</em></a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="cnavWrap">
			<ul class="cnav">
				<li class="cnavTop">
					<a href="https://www.benesse-hd.co.jp/en/ir/index.html"><em class="exb">Investor Relations</em></a>
				</li>
				<li class="cnavTxt">
					<ul>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/ir/strategy/index.html"><span>Management Policy</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/ir/strategy/index.html"><b>Management Policy</b></a>
										<div>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/en/about/message.html">Top Message</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/strategy/middleplan.html">Medium-Term Management Plan</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/about/management/governance.html">Governance</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/strategy/communication.html">Communication with Shareholders and Investors</a></li>
											</ul>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/en/ir/strategy/risk.html">Risk Factors</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/strategy/ir_policy.html">Basic IR Policy</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/strategy/award.html">Awards</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/strategy/creation_model.html">The Benesse Value Creation Model</a></li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/ir/ir_news/index.html"><span>IR News</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/ir/ir_news/index.html"><b>Latest Year</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/en/ir/ir_news/past.html?year=2021">2021</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/ir/ir_news/past.html?year=2020">2020</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/ir/ir_news/past.html?year=2019">2019</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/ir/ir_news/past.html?year=2018">2018</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/ir/finance/index.html"><span>Financial Data</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/ir/finance/index.html"><b>Financial Data</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/en/ir/finance/chart.html">Chart Generator</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/ir/finance/data_book.html">DATA BOOK</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/ir/finance/forecast.html">Earnings Forecasts</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/ir/finance/subsidiary.html">Major Subsidiaries and Associated Companies</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/ir/library/index.html"><span>Financial Reports</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/ir/library/index.html"><b>Financial Reports</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/en/ir/library/results/index.html">Financial Results &amp; Analyst Meeting</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/ir/library/ar/index.html">Integrated Report</a></li>
											<li><a href="https://www.benesse-hd.co.jp/en/ir/library/factsheet/index.html">Fact Sheet</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/ir/stock/index.html"><span>Stock Information</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/index.html"><b>Stock Information</b></a>
										<div>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/chart.html">Stock Prices</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/basic.html">General Information</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/return.html">Shareholder Return</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/shareholders_meeting.html">Shareholders Meeting</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/dividend.html">Dividends per Share</a></li>
											</ul>
											<ul class="cnavSub2">
												<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/split.html">Stock Split</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/repurchase.html">Share Buyback &amp; Treasury Stock Cancellation</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/bond.html">Corporate Bond &amp; Ratings</a></li>
												<li><a href="https://www.benesse-hd.co.jp/en/ir/stock/analyst.html">Analyst Coverage</a></li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/ir/individual/beginner.html"><span>Introducing the Benesse Group</span></a></strong></li>
						<li><strong><a href="https://blog.benesse.ne.jp/bh/en/ir_calendar/2020/"><span>IR Calendar</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/ir/support/index.html"><span>Support</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/ir/support/index.html"><b>Support</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/en/utility/faq.html">Frequently Asked Questions</a></li>
											<li><a href="https://enquete.benesse.ne.jp/forms/o/we7defe856/form.php" target="_blank">IR Contact</a><img class="blank" src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/icon/icon_blank.png" width="15" height="15" alt=""></li>
											<li><a href="https://www.benesse-hd.co.jp/en/utility/clause.html">Disclaimer</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</li>
				<li class="cnavThb">
					<ul>
						<li><a href="https://www.benesse-hd.co.jp/en/about/message.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana021_2021.jpg" width="170" height="180" alt=""></span><em>Top Message</em></a></li>
						<li><a href="https://www.benesse-hd.co.jp/en/ir/library/ar/2019/index.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/en/hd-common/images/nav/bana024.jpg" width="170" height="180" alt=""></span><em>New Medium-Term Management Plan</em></a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="cnavWrap">
			<ul class="cnav">
				<li class="cnavTop">
					<a href="https://www.benesse-hd.co.jp/en/sustainability/index.html"><em class="exb">Sustainability</em></a>
				</li>
				<li class="cnavTxt">
					<ul>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/sustainability/message/index.html"><span>Top Message</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/sustainability/vision/index.html"><span>Sustainability Vision</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/sustainability/management/index.html"><span>Sustainability Management</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/sustainability/management/index.html"><b>Sustainability Management</b></a>
										<ul class="cnavSub2">
											<li><a href="https://www.benesse-hd.co.jp/en/sustainability/framework/index.html">Promotion of Sustainability</a></li>
											<li><a href="/en/themes/141">Communication with Stakeholders</a></li>
											<li><a href="/en/themes/142">Participation in Initiatives</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/sustainability/materiality/index.html"><span>Materiality</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="https://www.benesse-hd.co.jp/en/sustainability/materiality/index.html"><b>Materiality</b></a>
										<ul class="cnavSub2">
											<li><a href="https://blog.benesse.ne.jp/bh/ja/sustainability/activity/index.html">Examples of Materiality action【Japanese】</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="/en/themes/145"><span>Benesse Group's ESG Initiatives</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="/en/themes/145"><b>Benesse Group's ESG Initiatives</b></a>
										<ul class="cnavSub2">
											<li><a href="/en/themes/146">Environment</a></li>
											<li><a href="/en/themes/151">Society</a></li>
											<li><a href="/en/themes/161">Governance</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="/en/themes/166"><span>Sustainability Data</span></a></strong>
							<div>
								<ul class="cnavSub">
									<li><a href="/en/themes/166"><b>Sustainability Data</b></a>
										<div>
											<ul class="cnavSub2">
												<li><a href="/en/themes/167">Overview of Benesse Holdings, inc.</a></li>
												<li><a href="/en/themes/168">ESG Data</a></li>
												<li><a href="/en/themes/169">GRI Standards Content Index</a></li>
											</ul>
											<ul class="cnavSub2">
												<li><a href="/en/themes/170">The United Nations' Global Compact</a></li>
												<li><a href="/en/themes/171">Integrated Report</a></li>
												<li><a href="/en/themes/172">All Sustainability links</a></li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
						</li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/sustainability/evaluation/index.html"><span>External Evaluations and Awards</span></a></strong></li>
						<li><strong><a href="https://www.benesse-hd.co.jp/en/sustainability/mirai/index.html" target="_blank"><span>Benesse Brighter Future for Children Booster</span></a></strong></li>
					</ul>
				</li>
				<li class="cnavThb">
					<ul>
						<li><a href="https://www.benesse-hd.co.jp/en/sustainability/vision/index.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/en/hd-common/images/nav/bana032.jpg" width="170" height="180" alt=""></span><em>Sustainability Vision</em></a></li>
						<li><a href="https://www.benesse-hd.co.jp/en/sustainability/materiality/index.html"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana033.jpg" width="170" height="180" alt=""></span><em>Materiality</em></a></li>
						<li><a href="/en/themes/145"><span><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/nav/bana034.jpg" width="170" height="180" alt=""></span><em>Benesse Group's<br>ESG Initiatives</em></a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="cnavWrap searchWrap" id="searchWrap1">

			<form class="searchFormCommonClass" id="ss-form1" action="https://www.benesse-hd.co.jp/en/search/" method="get" accept-charset="UTF-8">
				<input type="text" id="ss-query1" name="query" autocomplete="off" aria-autocomplete="none" placeholder="Type the keyword" class="searchInputCommonClass">
				<input type="hidden" name="charset" value="utf-8">

				<div class="ss-categories">
					<fieldset class="searchCategoryCommonClass ss-category-field">
						<div class="flex_bc">
							<input type="checkbox" name="category" value="1" class="ss-category" id="ss-category-default0-pc">
							<label for="ss-category-default0-pc">About Benesse</label>
							<input type="checkbox" name="category" value="2" class="ss-category" id="ss-category-default1-pc">
							<label for="ss-category-default1-pc">Investor Relations</label>
							<input type="checkbox" name="category" value="3" class="ss-category" id="ss-category-default2-pc">
							<label for="ss-category-default2-pc">Sustainability</label>
						</div>
					</fieldset>
				</div>

				<fieldset class="searchFiletypeCommonClass ss-category-field">
					<div class="flex_cc">
						<input type="radio" name="file_type" value="1" checked="checked" class="ss-category" id="ss-category-file_type0-pc">
						<label for="ss-category-file_type0-pc">All</label>
						<input type="radio" name="file_type" value="2" class="ss-category" id="ss-category-file_type1-pc">
						<label for="ss-category-file_type1-pc">HTML</label>
						<input type="radio" name="file_type" value="3" class="ss-category" id="ss-category-file_type2-pc">
						<label for="ss-category-file_type2-pc">PDF</label>
					</div>
				</fieldset>

				<button type="submit" class="searchButtonCommonClass ss-search-button ss_menu flex_cc"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path class="b" d="M18.26,14.22a4,4,0,1,1-2.78,1.11,4,4,0,0,1,2.78-1.11m0-3a7,7,0,1,0,5.12,2.21,7,7,0,0,0-5.12-2.21Z"></path><line x1="22.89" y1="23.1" x2="26.71" y2="27.39"></line></svg><span>Search</span></button>
			</form>
		</li>
	</ul>

	<nav id="menuWrap">
		<ul>
			<li></li>
		</ul>
		<ul class="unav">
			<li><a href="/en/index.html" id="lang2" class="Lar over"><span>Japanese</span></a></li>
		</ul>
	</nav>
	<div class="searchWrap" id="searchWrap2">

		<form class="searchFormCommonClass" id="ss-form2" action="https://www.benesse-hd.co.jp/en/search/" method="get" accept-charset="UTF-8">
			<input type="text" id="ss-query2" name="query" autocomplete="off" aria-autocomplete="none" placeholder="Type the keyword" class="searchInputCommonClass">
			<input type="hidden" name="charset" value="utf-8">

			<div class="ss-categories">
				<fieldset class="searchCategoryCommonClass ss-category-field">
					<div class="flex_bc">
						<input type="checkbox" name="category" value="1" class="ss-category" id="ss-category-default0-sp">
						<label for="ss-category-default0-sp">About Benesse</label>
						<input type="checkbox" name="category" value="2" class="ss-category" id="ss-category-default1-sp">
						<label for="ss-category-default1-sp">Investor Relation</label>
						<input type="checkbox" name="category" value="3" class="ss-category" id="ss-category-default2-sp">
						<label for="ss-category-default2-sp">Sustainability</label>
					</div>
				</fieldset>
			</div>

			<fieldset class="searchFiletypeCommonClass ss-category-field">
				<div class="flex_cc">
					<input type="radio" name="file_type" value="1" checked="checked" class="ss-category" id="ss-category-file_type0-sp">
					<label for="ss-category-file_type0-sp">All</label>
					<input type="radio" name="file_type" value="2" class="ss-category" id="ss-category-file_type1-sp">
					<label for="ss-category-file_type1-sp">HTML</label>
					<input type="radio" name="file_type" value="3" class="ss-category" id="ss-category-file_type2-sp">
					<label for="ss-category-file_type2-sp">PDF</label>
				</div>
			</fieldset>

			<button type="submit" class="searchButtonCommonClass ss-search-button ss_menu flex_cc"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path class="b" d="M18.26,14.22a4,4,0,1,1-2.78,1.11,4,4,0,0,1,2.78-1.11m0-3a7,7,0,1,0,5.12,2.21,7,7,0,0,0-5.12-2.21Z"></path><line x1="22.89" y1="23.1" x2="26.71" y2="27.39"></line></svg><span>Search</span></button>
		</form>
		<a class="x_Close"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><circle cx="20" cy="20" r="18.5"/><path d="M20,3A17,17,0,1,1,3,20,17,17,0,0,1,20,3m0-3A20,20,0,1,0,40,20,20,20,0,0,0,20,0Z"/><line x1="25.14" y1="15.09" x2="14.86" y2="24.91"/><line x1="15.09" y1="14.86" x2="24.91" y2="25.14"/></svg><span>Close</span></a>
	</div>
</header>
*/}.toString().match(/\/\*([^]*)\*\//)[1].replace(/\n|\r|\t/g,'');


var footerInc_ja = function(){/*
<footer id="footer">
	<nav id="foot">
		<div>
			<ul id="fnav1">
				<li><a href="https://www.benesse-hd.co.jp/ja/about/index.html">企業・グループ情報</a></li>
				<li><a href="https://blog.benesse.ne.jp/bh/ja/news/archives.html">お知らせ</a></li>
				<li><a href="https://www.benesse-hd.co.jp/ja/ir/index.html">投資家(IR)情報</a></li>
				<li><a href="https://www.benesse-hd.co.jp/ja/sustainability/index.html">サステナビリティ</a></li>
				<li><a href="https://www.benesse-hd.co.jp/ja/recruit/index.html">採用情報</a></li>
			</ul>
			<ul id="fnav2">
				<li><a href="https://www.benesse.co.jp/" target="_blank">ベネッセグループの商品・サービス</a></li>
				<li><a href="https://www.benesse-hd.co.jp/ja/utility/sitemap.html">サイトマップ</a></li>
				<li><a href="https://www.benesse-hd.co.jp/ja/utility/contact.html">よくあるご質問・お問い合わせ</a></li>
				<li><a href="https://www.benesse.co.jp/customer/" target="_blank">ベネッセお客様本部</a></li>
			</ul>
		</div>
		<ul id="fsocial">
			<li><a href="https://www.youtube.com/channel/UCgLcwxhweKoLSXSGos06Tsg" target="_blank"><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/icon/youtube.png" alt=""><span>ベネッセ Youtube<br>公式チャンネル</span></a></li>
			<li><a href="https://www.benesse.co.jp/campaign/socialmedia.html" target="_blank"><img src="//sustainability-cms-benesse-hd-s3.s3-ap-northeast-1.amazonaws.com/ja/hd-common/images/icon/social.png" alt=""><span>ソーシャルメディア<br>公式アカウント一覧</span></a></li>
		</ul>
	</nav>
	<ul id="fnav3">
		<li><a href="https://www.benesse-hd.co.jp/ja/utility/term.html">サイトのご利用にあたって</a></li>
		<li><a href="https://www.benesse-hd.co.jp/ja/utility/privacy.html">個人情報保護について</a></li>
		<li><a href="https://www.benesse-hd.co.jp/ja/about/management/antisocial.html">反社会的勢力排除に関する基本方針</a></li>
		<li><a href="https://www.benesse-hd.co.jp/ja/utility/notice.html">電子公告</a></li>
		<li><a href="https://www.benesse-hd.co.jp/ja/utility/airticle.html">定款</a></li>
		<li><a href="https://www.benesse-hd.co.jp/ja/utility/link.html">関連リンク</a></li>
	</ul>
	<p>Copyright &copy; Benesse Holdings, Inc. All rights reserved.</p>
</footer>
*/}.toString().match(/\/\*([^]*)\*\//)[1].replace(/\n|\r|\t/g,'');


var footerInc_en = function(){/*
<footer id="footer">
	<nav id="foot">
		<div>
			<ul id="fnav1">
				<li><a href="https://www.benesse-hd.co.jp/en/about/index.html">About Benesse</a></li>
				<li><a href="https://www.benesse-hd.co.jp/en/ir/index.html">Investor Relations</a></li>
				<li><a href="https://www.benesse-hd.co.jp/en/sustainability/index.html">Sustainability</a></li>
			</ul>
			<ul id="fnav2">
				<li><a href="https://www.benesse-hd.co.jp/en/utility/sitemap.html">Site Map</a></li>
				<li><a href="https://www.benesse-hd.co.jp/en/utility/contact.html">FAQ/Contact Us</a></li>
			</ul>
		</div>
	</nav>
	<ul id="fnav3">
		<li><a href="https://www.benesse-hd.co.jp/en/utility/term.html">Terms and Conditions</a></li>
		<li><a href="https://www.benesse-hd.co.jp/en/utility/privacy.html">Privacy Policy</a></li>
		<li><a href="https://www.benesse-hd.co.jp/en/about/management/antisocial.html">Basic Stance and Measures to Eliminate Antisocial Forces</a></li>
		<li><a href="https://www.benesse-hd.co.jp/en/utility/airticle.html">Articles of Incorporation</a></li>
	</ul>
	<p>Copyright &copy; Benesse Holdings, Inc. All rights reserved.</p>
</footer>
*/}.toString().match(/\/\*([^]*)\*\//)[1].replace(/\n|\r|\t/g,'');




})();

